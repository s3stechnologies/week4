
public class SwitchExample {
	
	
	

	public static void main(String[] args) {
		
		//int n = 1;
		
		
		char a = 'A';
		
		System.out.println(Character.getNumericValue(a));
		
		
		String abc = "Monday";
		
		//double num = 0.0;
//		
//		if(n == 1 ) {
//			System.out.println("1");
//
//		}else if (n == 2) {
//			System.out.println("2");
//		}else if (n == 3) {
//			System.out.println("3");
//		}else if (n == 4) {
//			System.out.println("4");
//		}else if (n == 5) {
//			System.out.println("5");
//		}else {
//			System.out.println("Default value : " + "0");
//		}
		
		
		
		// int , String , Enums ( since JVM identifies char as an int or converts it to an int value, that's why we can use Char as well 
		
		
		int n = 1;
		
		switch(n) {
		
		case 1: case 2: case 3:
		System.out.println("1");
		break;
//		
//		case 2:
//		System.out.println("2");
//		break;
//		
//		case 3: 
//		System.out.println("3");
//		break;
		
		case 4:
		System.out.println("4");
		break;
		
		case 5:
		System.out.println("5");
		break;
		
		default:
			System.out.println("Default value : " + "0");
		
		}


	}

}
