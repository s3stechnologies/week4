
interface EnumExa{
	
	
	String NAME = "Biwash";
	 
	 int VALUE = 1;
	
		
}


class BeforeEnums {
	
	public static final String NAME = "Biwash";
	
}

enum AfterEnum{
	
	 BIWASH(1),ARADHANA(2), KRITEE(3),RAJENDRA(7), RAJSHREE(4), SAMJHAN(5), SURAJ(6);
	
	
	private int indexNumberOfEnum;

	
	// I can have extra methods
	public String myName() {
		return "Biwash";
	}
	public String myName(String name) {
		return "Bohara";
	}
	
	private AfterEnum(int type) {
		this.indexNumberOfEnum = type;
	}

	public int getType() {
		return indexNumberOfEnum;
	}

	public void setType(int type) {
		this.indexNumberOfEnum = type;
	}
	
	
	
}



public class EnumExample {
	
//enum EnumExa{
//		
//	}
	

	public static void main(String[] args) {
	
		AfterEnum enumExam = AfterEnum.RAJENDRA;
		
		
		
		System.out.println(enumExam.getType());
		
		
		
//		String name = AfterEnum.BIWASH.toString();
//		
//		// enum in Switch
//		switch(enumExam) {
//		
//		case BIWASH:
//			System.out.println("Biwash");
//			break;
//		case ARADHANA:
//			System.out.println("Biwash");
//			break;
//	
//		}
		
		
		
		

		
	}

}
