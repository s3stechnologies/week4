
public class ForExample {

	
	
	
	public static void main(String[] args) {
		
//		int i = 0; first
//		
//		(i <=5) second
//		 
//		i++  third
		
		//int i;
		
		//int limit = 3;
//		
//		for( int i = 1; i <= 5; i++) {
//			
//			if( i == limit) {
//				
//				break; // lable
//				//
//				//
//			}
//			//code goes here
//			System.out.println("Hello World!");
//			//
//			//
//		}
		
//		int limit = 3;
//		
//		for( int i = 1; i <= 5; i++) {
//			
//			if( i == limit) {
//				continue; // lable
//			}
//			System.out.println("Hello World!" + i);
//		}
		
		
//		int sum = i + 6;
//		
//		System.out.println(sum);
		
		//b = b +2;
		// b = b-2;
		
//		for(int a = 0, b = 2; b - a < 5 ; a++, b+=3) {
//			
//		//	System.out.println("value of a is :"+ a + " " +"Value of b is: " + b);
//			
//		}
//		
//		
//		String[] name = {"Ram","Shyam","Hari", "Radhe"};
//		
//	
//		
//		double[] decimalNumber = { 0.0, 1.0, 2.0};
		
//		System.out.println(name[0]);
//		System.out.println(name[1]);
//		System.out.println(name[2]);
		
		
//		for (int i = 0; i< name.length; i++) {
//			
//			
//			System.out.println(name.length);
//			System.out.println(name[i]);
//			
//		}
//		
		
//		int[] number = {1,2,3,4,5};
//		
//		for(int num : number) {
//			
//			System.out.println(num);
//		}
//		
//		//System.out.println(num);
//		//int num  = "Name";
//		
//		// for each loop 
//		// most used loop in Java
//		// varibale type : array that you are iterating over
//		for(String name1 : name) {
//			System.out.println(name1.concat(" Bohara"));
//		}
		
		
		
		
		//Lable
		
		int a = 2;
		
		outer:
		for(int i = 0; i < 5; i ++) {
			
			System.out.println("I before entering inner loop : " + i);
			
			for(int j = 0; j < 3 ; j++) {
				
				if(i == a) {
					break outer;
					// code here not going to run
				}
				System.out.println("J : " +j);
				
				System.out.println(" I inside of  inner loop :" + i);
			}
			
			System.out.println("I outside of  inner loop :" + i);
		}
		
		outer:
			for(int i = 0; i < 5; i ++) {
				
				System.out.println("I before entering inner loop : " + i);
				for(int j = 0; j < 3 ; j++) {
					
					if(i == a) {
						continue outer;
						//code here not going to run
					}
					System.out.println("J : " +j);
					
					System.out.println(" I inside of  inner loop :" + i);
				}
				
				System.out.println("I outside of  inner loop :" + i);
			}
		
		
		
		
			for(int i = 0; i < 5; i ++) {
				
				System.out.println("I before entering inner loop : " + i);
				inner:
				for(int j = 0; j < 3 ; j++) {
					
					if(i == a) {
						continue inner;
						//code here not going to run
					}
					System.out.println("J : " +j);
					
					System.out.println(" I inside of  inner loop :" + i);
				}
				
				System.out.println("I outside of  inner loop :" + i);
			}
			
			
			for(int i = 0; i < 5; i ++) {
				
				System.out.println("I before entering inner loop : " + i);
				inner:// it;s right above the for loop, any line between the inner and the for loop is going to through an error
				//	System.out.println("inner label throws an error");
				for(int j = 0; j < 3 ; j++) {
					
					if(i == a) {
						break inner;
						//code here not going to run
					}
					System.out.println("J : " +j);
					
					System.out.println(" I inside of  inner loop :" + i);
				}
				
				System.out.println("I outside of  inner loop :" + i);
			}
		

	}

}
